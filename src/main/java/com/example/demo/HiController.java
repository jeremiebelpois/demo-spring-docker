package com.example.demo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HiController {
    @GetMapping("/id/{id}")
    public int getById(@PathVariable("id") int id){
        return id;
    }
@GetMapping("/email/{email}")
    public String getByEmail(@PathVariable("email") String email){
        return email;
    }
}
